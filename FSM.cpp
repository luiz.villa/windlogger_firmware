/**
 *******************************************************************************
 *******************************************************************************
 *
 *	License :
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *******************************************************************************
 *******************************************************************************
 *
 *
 *    @file   FSM.h
 *    @author gilou
 *    @date   19 févr. 2018
 *    @brief  The FSM is the finish state machine mechanism.
 *
 *    This is the Final State Machine organization
 *
 */

// Initial includes

// Basic Includes
#include "Arduino.h"    // The general arduino commands which are compatible with the Windlogger
#include "FSM.h"        // The FSM header with all the class definitions of the final state machine of the Windlogger 

// Hardware includes
#include "Rtc_Pcf8563.h"
#include "SD.h"

// Sensor includes
#include "Anemometer.h"
#include "Windvane.h"

//**************************************************************************************************************************
// THIS IS THE WRONG WAY OF DECLARING OBJECTS IN A CPP PROGRAM. THESE DECLARATIONS SHOULD EVENTUALLY BE MOVED TO THE .H FILE. 
//**************************************************************************************************************************

Anemometer anemo1(0);	// Anemo1 is collected in the timer0 
Anemometer anemo2(1);	// Anemo2 is collected in the timer1
Windvane vane(6);		// The windvane is connected to the ADC6

/******************************************************************************
 * Constructor and destructor definition
 */
FSM::FSM() {


}	// FSM constructor


FSM::~FSM() {}	// FSM destructor

/******************************************************************************
 * Public flags
 */
bool FSM::flag_configRequest = false;			/**< initializes the flag_configRequest to false (no config request) */
bool FSM::flag_frequenciesReady = false;		/**< initializes the flag_frequenciesReady to false (no measurement to be read) */
bool FSM::flag_measure = false;					/**< initializes the flag_measure to false (no measurement to start) */

/******************************************************************************
 * State machine mechanic methods
 */
/// todo bug in the FSM, if no anemo, never the averages are calculated... loop around st_measure
// this method initialize the FSM
void FSM::init(){
	m_eeprom_addr = 0;				// data are store from the 0 adress onwards
	nextState = &FSM::st_SETUP;		// the first state is : st_setup

	load_param();					// calls the method that loads the values of the FSM parameters from EEPROM

	rtc.getDateTime();				// calls the method that uploads date and time from the RTC

	anemo1.load_param();            // calls the method that loads the parameters of the anemometer 1
	anemo2.load_param();            // calls the method that loads the parameters of the anemometer 2

	vane.load_param();              // calls the method that loads the parameters of the wind vane

	power1.load_param();            // calls the method that loads the parameters of the power measurement 1 (DC or AC)
	power2.load_param();            // calls the method that loads the parameters of the power measurement 2 (DC or AC)

	sd_init = false;                // sets the sd initialize flag as false

}

void FSM::timingControl(){
	// update local variables
	rtc.getTime();

	if(secondOld!=rtc.getSecond()){				// is it a new second ?
		second_counter++;						// Use a second counter to compare with the measurePeriode

		if((second_counter%measurePeriode)==0)
		{
			//Serial.print(measurePeriode); Serial.println("new measure");
			flag_measure = true;				// if it's true, we can do a new measure
			second_counter=0;					// reset the counter
		}
	secondOld = rtc.getSecond();		// update timestamp_old
	}

	// get anemo flag value
	flag_frequenciesReady = anemo1.flag_anemo();
	//Serial.print(anemo1.flag_anemo());
}

/******************************************************************************
 * Configuration management
 ******************************************************************************/
//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: menu
// Decription:  Prints the commands of the configuration menu  
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::menu(){
	Serial.println("Configuration menu :");
	Serial.println("	$1 - FSM");
	Serial.println("	$2 - Date/Time");
	Serial.println("	$3 - Anemo1");
	Serial.println("	$4 - Anemo2");
	Serial.println("	$5 - Windvane");
	Serial.println("	$6 - Power1");
	Serial.println("	$7 - Power2");

	Serial.println("	$9 - Output configuration");
}

void FSM::printConfig(){
	Serial.println("FSM config stuff :");
	Serial.print("	*11 measure_sample_conf = ");	Serial.print(measureSampleConf);	Serial.println("		0: no measure,1: 10s average,2:1min average,3:10min average.");
	Serial.print("	*12 node id = ");	Serial.print(node_id);	Serial.println("		permit identify each datalogger (0 - 255).");
	Serial.print("	*19 Reset configuration - reinitialize the FSM and each sensors configuration.");
}

void FSM::printDateTime(){
	Serial.println("Date/Time config :");
	Serial.print("	*21 Time : ");	Serial.println(rtc.formatTime());
	Serial.print("	*22 Date : ");	Serial.println(rtc.formatDate());
}

bool FSM::config(char *stringConfig){
	uint8_t item = stringConfig[2]-'0';	// convert item in char

	double arg_f = atof(stringConfig + 4);	// convert the second part, the value in double to cover all possibilities.
	unsigned char arg_uc = (unsigned char)arg_f;
	switch (item) {
		case 1:	// choose measurement periode parameter
			switch (arg_uc) {
			case 0:	// no measurement, use at the first wake up
				measureSampleConf = 0;measureMax = 0;measurePeriode = 0;
				update_param();	measure = 0; second_counter = 0;
				break;
			case 1:	// Config 1 : 2 measures in 10 secondes
				measureSampleConf = 1;measureMax = 2;measurePeriode = 5;
				update_param();	measure = 0; second_counter = 0;
				break;
			case 2:	// Config 2 : 4 measures in 1 minute
				measureSampleConf = 2;measureMax = 4;measurePeriode = 15;
				update_param();	measure = 0; second_counter = 0;
				break;
			case 3:	// Config 3 : 10 measures in 10 minutes
				measureSampleConf = 3;measureMax = 10;measurePeriode = 60;
				update_param();	measure = 0; second_counter = 0;
				break;
			default:
				Serial.println("Not a correct value");
				break;
			}
		break;
		case 2:	// Set the Node id number
			node_id = arg_uc;
			update_param();
			break;
		case 9:	// Reset each param,add here each sensors initialise_param method
			initialize_param();		// initialize the FSM
			anemo1.initialize_param();
			anemo2.initialize_param();
			vane.initialize_param();
			power1.initialize_param();
			power2.initialize_param();
			break;
		default:
			Serial.print("Bad request : ");Serial.println(item);
	}

	return 0;
}

void FSM::configDT(char *stringConfig){
	// valid string are :
	//	*21=hh:mm:ss
	//	*22=mm/dd/yyyy
	//	else bad request
	uint8_t item = stringConfig[2]-'0';	// convert item in char

	switch (item) {
		case 1:	//	*21=hh:mm:ss
			if(stringConfig[6]==':'&&stringConfig[9]==':'){	// test time separator
				char hours[3]={stringConfig[4],stringConfig[5],'\0'};
				char mins[3]={stringConfig[7],stringConfig[8],'\0'};
				char secs[3]={stringConfig[10],stringConfig[11],'\0'};

				rtc.setDateTime(rtc.getDay(), rtc.getWeekday(), rtc.getMonth(), 0,  rtc.getYear(), atoi(hours), atoi(mins), atoi(secs));
			}
			else Serial.print("Bad value : type *21=hh:mm:ss");
			break;
		case 2://	*22=mm/dd/yyyy
			if(stringConfig[6]=='/'&&stringConfig[9]=='/'){	// test date separator
				char months[3]={stringConfig[4],stringConfig[5],'\0'};
				char days[3]={stringConfig[7],stringConfig[8],'\0'};
				char years[3]={stringConfig[12],stringConfig[13],'\0'};

				rtc.setDateTime(atoi(days), rtc.getWeekday(), atoi(months), 0, atoi(years), rtc.getHour(), rtc.getMinute(), rtc.getSecond());
			}
			else Serial.print("Bad value : type *22=mm/dd/yyyy");
			break;
		default:
			Serial.print("Bad request : ");Serial.print(item);
			break;
	}
}

void FSM::configOutput(char *stringConfig){
	uint8_t item = stringConfig[2]-'0';	// convert item in char

	double arg_f = atof(stringConfig + 4);	// convert the second part, the value in double to cover all possibilities.
	unsigned char arg_uc = (unsigned char)arg_f;
	switch (item) {
		case 1:	// enable or disable write data on Serial
			if(arg_uc==0)serial_enable = false;	// disable
			else serial_enable = true;				// enable
			update_param();
		break;
		case 2:	// enable or disable write data on SD card
			if(arg_uc==0)sd_enable = false;	// disable
			else sd_enable = true;				// enable
			update_param();
			break;
//			case 3:	// Set offset value
//				m_offset = arg_f;
//				update_param();
//				break;
		default:
			Serial.print("Bad request : ");Serial.println(item);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: printOutput
// Decription:  Prints information about the enabled output of the datalogger. Add new output peripherals here.  
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::printOutput(){
	Serial.println("Output config :");                                      // prints the title
	Serial.print("	*91 Serial enable : ");	Serial.println(serial_enable);  // prints if the serial is enabled
	Serial.print("	*92 Sd card enable : ");	Serial.println(sd_enable);  // prints if the sd card is enabled
}

/******************************************************************************
 * State list declaration
 */

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_SETUP
// Decription:  Executes the initial setup of the datalogger 
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_SETUP(){
#ifdef DEBUG_FSM
	Serial.println("st_SETUP");
#endif

	// by default the transition is ev_waiting
	ev_isWaiting();         // This event is a flag that calls the st_SLEEP state
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_CONFIG
// Decription:  Runs the config state 
// Transition priorities:   1       | 2    
//                          config  | sleep
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_CONFIG(){

#ifdef DEBUG_FSM
	Serial.println("st_CONFIG");
#endif

	if(flag_configRequest==true){
		Serial.println(serialString);

		switch (serialString[0]) {
			case '$':                       // A "$" sign denotes a reading procedure from the menu. 
				// menu config
				switch (serialString[1]) {
					case '$':               // A second $ sign denotes a change in the settings
						isInConfig = 1;		// Sets inInConfig flag to one, keeping the datalogger in the same state
						menu();             // Prints the commands for the configuration menu
						break;
					case '1':               // a "1" calls the print of the current config values
						printConfig();
						break;
					case '2':
						printDateTime();    // a "2" calls the print of the date and time values
						break;
					case '3':
						anemo1.print_config();  // a "3" calls the print of the anemometer 1 configuration values
						break;
					case '4':
						anemo2.print_config();  // a "4" calls the print of the anemometer 2 configuration values
						break;
					case '5':
						vane.print_config();    // a "5" calls the print of the wind vane configuration values
						break;
					case '6':
						power1.print_config();  // a "6" calls the print of the configuraiton values for the power calculation 1 
						break;
					case '7':
						power2.print_config();  // a "7" calls the print of the configuration values for the power calcualtion 2
						break;

					case '9':                  
					printOutput();              // a "9" calls the print of the Output values
					break;
					case 'q':                   // a "q" closes the configuration and starts the measurements
						Serial.println("Config Done and start measurement.");
						isInConfig = 0;	                                        // desables the flag that shows the code is in configuration mode
						measure = 0; second_counter = 0;	                    // resets the measurements and the counter of secondsmeasure
						break;
					default:                    // anything else gets a return as a "bad request"
						Serial.println("Bad request");                          
						break;
				}
				break;
			case '*':              // a "*" denotes a writing routine into the config values
				// Sub Config sender
				switch (serialString[1]) {
					case '1':                   // a "1" denotes writing the data onto the config
						config(serialString);   
						printConfig();          
						break;
					case '2':                   // a "2" denotes writing onto the date and time config
						configDT(serialString); 
						printDateTime();        
						break;
					case '3':                           // a "3" denotes writing onto the anemometer 1 config
						anemo1.config(serialString);
						anemo1.print_config();
						break;
					case '4':                           // a "4" denotes writing onto the anemometer 2 config
						anemo2.config(serialString);
						anemo2.print_config();
						break;
					case '5':                           // a "5" denotes writing onto the wind vane config
						vane.config(serialString);
						vane.print_config();
						break;
					case '6':                           // a "6" denotes writing onto the power 1 config
						power1.config(serialString);
						power1.print_config();
						break;
					case '7':                           // a "7" denotes writing onto the power 2 config
						power2.config(serialString);
						power2.print_config();
						break;

					case '9':                           // a "9" denotes writing onto the output config (serial, sd, wifi, etc...)
						configOutput(serialString);
						printOutput();
						break;
					default:                            // anything else is seen as a "bad request"
						Serial.println("Bad request");
						break;
				}
				break;
			default:
				Serial.println("Bad request");          // if neither a "$" nor a "*" is given, the code returns a "bad request"
				break;
		}
		StrIndex=0;                                     // resets the string index
		flag_configRequest = false;                     // resets the config request flag

		if(!(serialString[0]=='$' && serialString[1]=='q' ))	                    // reminds the user how to quit ($q) or how to keep configuring ($$)
				Serial.println("Main menu : $$ or Quit and start measurement : $q");
	}

    //--------------------------------------------------
	// Transition tests
    //--------------------------------------------------
    
	if(flag_configRequest || isInConfig>0) ev_configRequest();          // stays in the config status is still active, it loops (triggers the ev_configRequest that calls the st_CONFIG)
	else ev_isWaiting();	                                            // by default the transition is ev_waiting

}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_SLEEP
// Decription:  Runs the sleep state 
// Transition priorities:   1       | 2         | 3                 | 4
//                          config  | measure   | read_frequencies  | sleep
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_SLEEP(){
#ifdef DEBUG_FSM
	//Serial.println("st_SLEEP");
#endif


    //--------------------------------------------------
	// Transition tests
    //--------------------------------------------------
    
	if(flag_configRequest) ev_configRequest();              // Priority 1 - this event calls the st_CONFIG state
	else if(flag_measure) {                                                         
		ev_measure();                                       // Priority 2 - this event calls the st_MEASURE state
	}                                                                               
	else if(flag_frequenciesReady)ev_frequenciesReady();    // Priority 3 - this event calls the st_READ_FREQUENCIES state
	else ev_isWaiting();	                                // Priority 4 - (default) this event calls the st_SLEEP state (loops)
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_MEASURE
// Decription:  Runs the measurement state 
// Transition priorities:   1       | 2                 | 3
//                          config  | read_frequencies  | sleep
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_MEASURE(){

#ifdef DEBUG_FSM
	Serial.println("st_MEASURE");
#endif
	digitalWrite(LED_BUILTIN,HIGH);			// led on

	anemo1.start();	// start anemo1 and 2

	vane.read_value(measure);				// read the windvane value

	power1.read_value(measure, 4, 300);		// read power value
	power2.read_value(measure, 4, 300);


	digitalWrite(LED_BUILTIN,LOW);			// led off

	// reset the flag
	flag_measure = false;
	measure++;				// increase measure

    //--------------------------------------------------
	// Transition tests
    //--------------------------------------------------
	if(flag_configRequest) ev_configRequest();                              // calls the config state
	else if(flag_frequenciesReady)ev_frequenciesReady();                    // calls the 
	else ev_isWaiting();	// by default the transition is ev_waiting
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_READ_FREQUENCIES
// Decription:  Reads the frequency measurement from the anemometers 
// Transition priorities:   1       | 2            
//                          config  | calc_averages
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_READ_FREQUENCIES(){
#ifdef DEBUG_FSM
	Serial.println("st_READ_FREQUENCIES");
#endif

	anemo1.read_value(measure); 
	anemo2.read_value(measure);

	flag_frequenciesReady = false;             //resets the flag dedicated to the anemometer measurement

    //--------------------------------------------------
	// Transition tests
    //--------------------------------------------------
	if(flag_configRequest) ev_configRequest();        // either there's a configuration to be set or the code proceeds to calculate the averages
	else ev_testCounter();
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_CALC_AVERAGES
// Decription:  Calculates the averages of all the measurements. The average calculation is triggered once the code reaches the 
//              maximum number of measurements set by the user. 
// Transition priorities:   1       | 2            
//                          config  | calc_averages
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_CALC_AVERAGES(){
#ifdef DEBUG_FSM
	Serial.println("st_CALC_AVERAGES");
#endif

	
	bool isMeasureMax = false;
	if(measure == measureMax){              // Tests the number of measurements.It it is equal to measureMax, the code starts the average process
		isMeasureMax = true;

		anemo1.calc_average(measureMax);         
		anemo2.calc_average(measureMax);    
		vane.calc_average(measureMax);      
		power1.calc_average(measureMax);        
		power2.calc_average(measureMax);    

		timestamp = rtc.getTimestamp();	    // save the timestamp at the average calculation moment
		measure = 0;	                    // restart a new measurement sequence

		anemo1.clear(measureMax);
		anemo2.clear(measureMax);
		vane.clear(measureMax);
		power1.clear(measureMax);
		power2.clear(measureMax);
	}

    //--------------------------------------------------
	// Transition tests
    //--------------------------------------------------
    
	if(flag_configRequest) ev_configRequest();
	else if(isMeasureMax)ev_transmitting();
	else ev_isWaiting();
}

void FSM::st_OUTPUT(){
#ifdef DEBUG_FSM
	Serial.println("st_OUTPUT");
#endif
	bool isTransmitting = true;

	if(serial_enable==true){
		char dataString[50];

		// print on Serial (uart 0)
		Serial.print(timestamp); Serial.print("	");
		Serial.print(node_id); Serial.print("	");
		Serial.print(anemo1.get_average()); Serial.print("	");
		Serial.print(anemo2.get_average()); Serial.print("	");
		Serial.print(vane.get_average());	Serial.print("	");
		Serial.print(power1.get_average(dataString));	Serial.print("	");
		Serial.print(power2.get_average(dataString));	Serial.print("	");

		Serial.println();
	}

	if(sd_enable==true){
		// todo improve with SD card detection, SD_CD on pin 3
		char tempString[50];
		char dataString[200];

		strcpy(dataString, ltoa(timestamp, tempString, 10)); strcat(dataString,"	");
		strcat(dataString,itoa(node_id, tempString, 10)); strcat(dataString,"	");
		strcat(dataString,dtostrf(anemo1.get_average(), 1, 1, tempString)); strcat(dataString,"	");
		strcat(dataString,dtostrf(anemo2.get_average(), 1, 1, tempString)); strcat(dataString,"	");
		strcat(dataString,itoa(vane.get_average(), tempString, 10)); strcat(dataString,"	");
		strcat(dataString,power1.get_average(tempString)); strcat(dataString,"	");
		strcat(dataString,power2.get_average(tempString)); strcat(dataString,"	");

		if(sd_init==false){	// if sd is not initialize, do it
			Serial.print("Initializing SD card...");
			// make sure that the default chip select pin is set to
			// output, even if you don't use it:
			pinMode(10, OUTPUT);

			// see if the card is present and can be initialized:
			if (!SD.begin(SD_CS)) {
			Serial.println("Card failed, or not present");
			// don't do anything more:
			sd_init=false;
			}
			else {
				Serial.println("card initialized.");
				sd_init = true;
			}
		}

		if(sd_init==true)	// sd card is initialize, write on
		{
			// set file name from month and day
			char fileName[13]; char tempConv[6];
			strcpy(fileName,itoa(rtc.getMonth(),tempConv,10));
			strcat(fileName,itoa(rtc.getDay(),tempConv,10));
			strcat(fileName,".txt");

			// open the file. note that only one file can be open at a time,
			// so you have to close this one before opening another.
			File dataFile = SD.open(fileName, FILE_WRITE);

			// if the file is available, write to it:
			if (dataFile) {
			dataFile.println(dataString);
			dataFile.close();
			// print to the serial port too:
			//Serial.println(dataString);
			}
			// if the file isn't open, pop up an error:
			else {
			Serial.println("error opening datalog.txt");
			}
		}

	}

	isTransmitting = false;

	// Transition test ?
	if(isTransmitting) ev_transmitting();
	else ev_isWaiting();
}

/******************************************************************************
 * Eeprom management
 */
//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: load_param
// Decription:  Loads information from the EEPROM. If there's an incompatibility between the structure version and the data 
//              structure version, then it calls the initialize_param() method.
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::load_param(){
	structure_version = eeprom_read_byte((const unsigned char*)m_eeprom_addr);                  // loads the current version of the code
	if(structure_version != DATA_STRUCTURE_VERSION) initialize_param();                         // checks that the curent data structure is the same as the current version of the code
	else{
		node_id = eeprom_read_byte((const unsigned char*)m_eeprom_addr+5);                      // loads the node_id parameter - the id of the datalogger
		measureSampleConf = eeprom_read_byte((const unsigned char*)m_eeprom_addr+7);            // loads the measureSambleConf parameter - defines the sample rate (1 second, 1 minute, 10 minutes)
		measureMax = eeprom_read_byte((const unsigned char*)m_eeprom_addr+9);                   // loads the measureMax parametre - defines the maximum number
		measurePeriode = eeprom_read_byte((const unsigned char*)m_eeprom_addr+11);              // loads the measureMax parametre - defines the measurement rate in seconds

		if(eeprom_read_byte((const unsigned char*)m_eeprom_addr+13)==0) serial_enable = false;  // loads the serial_enable paramter - turns on the serial port. Since this is a boolean, it is loaded through an if that sets a boolean variable
		else serial_enable = true;
		if(eeprom_read_byte((const unsigned char*)m_eeprom_addr+15)==0) sd_enable = false;      // loads the sd_enable paramter - turns on the SD card. Since this is a boolean, it is loaded through an if that sets a boolean variable
		else sd_enable = true;

	}
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: update_param
// Decription:  Updates information onto the EEPROM. 
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::update_param (){
	eeprom_update_byte((unsigned char*)m_eeprom_addr, structure_version);                   // writes the structure_version parameter - the value of the data structure version
	eeprom_update_byte((unsigned char*)m_eeprom_addr+5, node_id);                           // writes the node_id parameter - 
	eeprom_update_byte((unsigned char*)m_eeprom_addr+7, measureSampleConf);                 // writes the measureSampleConf parameter
	eeprom_update_byte((unsigned char*)m_eeprom_addr+9, measureMax);                        // writes the measureMax parameter
	eeprom_update_byte((unsigned char*)m_eeprom_addr+11, measurePeriode);                   // writes the measurePeriod parameter

	if(serial_enable==false) eeprom_update_byte((unsigned char*)m_eeprom_addr+13, 0);	// writes the serial_enable parameter - Since this is a boolean parameter, it is written through an if. 0 if the serial is off and 1 if the serial is on.
	else eeprom_update_byte((unsigned char*)m_eeprom_addr+13, 1);						
	if(sd_enable==false) eeprom_update_byte((unsigned char*)m_eeprom_addr+15, 0);	    // writes the sd_enable parameter - Since this is a boolean parameter, it is written through an if. 0 if the sd is off and 1 if the sd is on.
	else eeprom_update_byte((unsigned char*)m_eeprom_addr+15, 1);						

}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: initialize_param
// Decription:  Initialize the eeprom memory. This method is called automatically whenever there's an incompatibility between the 
//              structure version and the data structure version
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::initialize_param (){
	eeprom_update_byte((unsigned char*)m_eeprom_addr, DATA_STRUCTURE_VERSION);          // writes the structure_version parameter - the value of the data structure version
	eeprom_update_byte((unsigned char*)m_eeprom_addr+5, 15);                            // writes the node_id parameter - sets the id value that identifies the datalogger as 15 by default
	eeprom_update_byte((unsigned char*)m_eeprom_addr+7, 0);                             // writes the measureSampleConf parameter - sets the measurement timing as 0 by default
	eeprom_update_byte((unsigned char*)m_eeprom_addr+9, 0);                             // writes the measureMax parameter - sets the maximum number of measurements to be performed by the code as 0 by default 
	eeprom_update_byte((unsigned char*)m_eeprom_addr+11, 0);                            // writes the measurePeriod parameter - defines the interval between two measurements in seconds
	eeprom_update_byte((unsigned char*)m_eeprom_addr+13, 1);                            // writes the serial_enable parameter - sets the serial as active by default
	eeprom_update_byte((unsigned char*)m_eeprom_addr+15, 0);                            // writes the sd_enable parameter - sets the sd card as off by default

	load_param();
}
