/**
 *******************************************************************************
 *******************************************************************************
 *
 *	License :
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *******************************************************************************
 *******************************************************************************
 *
 *
 *    @file   FSM.h
 *    @author gilou
 *    @date   19 févr. 2018
 *    @brief  The FSM is the finish state machine mechanism.
 *
 *    This is the Final State Machine organization
 *
 */


#ifndef FSM_H_
#define FSM_H_

//#define DEBUG_FSM

#include "Arduino.h"

#include "Rtc_Pcf8563.h"
#include "SD.h"
#include "Power.h"

class FSM;

typedef void (FSM::*_nextState)(void);		/**< This typedef define a method pointer type */


/**
 * \brief The class FSM regroup hardware peripherals declaration, mechanic methods and useful parameters.
 */
class FSM {
public:
	/******************************************************************************
	 * Constructor and destructor definitions
	 */
	FSM();
	virtual ~FSM();
	/******************************************************************************
	 * State machine mechanic methods
	 */
	/**
	 * \brief This method initializes the state machine
	 */
	void init();
	/**
	 * \brief This method is used to test each if a second has passed and if it the case it sets the "flag_measure" to show that the time has passed
	 */
	void timingControl();
	/******************************************************************************
	 * State pointer
	 */
	_nextState nextState = NULL;			/**< This method pointer is use to manipulate the next state function */

	/******************************************************************************
	 * Public flags
	 */
	static bool flag_frequenciesReady;		/**< Set when the timer3 overflow, it means that the data is ready to be retrieved*/
	static bool flag_configRequest;			/**< set when a string from usart0 finish by "\r", which denotes a change in configuration settings */
	static bool flag_measure;				/**< set when time has come to trigger a new measurement */

	/******************************************************************************
	 * transitions list
	 * Each transition for a next state is organized through events. Each event calls a method that sets the next state of the FSM.
	 */
	inline void ev_isWaiting(){nextState = &FSM::st_SLEEP;}						///< This event sets the nextState as st_SLEEP */
	inline void ev_testCounter(){nextState = &FSM::st_CALC_AVERAGES;}			///< This event sets the nextState as st_CALC_AVERAGES */
	inline void ev_transmitting(){nextState = &FSM::st_OUTPUT;}					///< This event sets the nextState as st_OUTPUT */
	inline void ev_configRequest(){nextState =&FSM::st_CONFIG;}					///< This event sets the nextState as st_CONFIG */
	inline void ev_measure(){nextState = &FSM::st_MEASURE;}						///< This event sets the nextState as st_MEASURE */
	inline void ev_frequenciesReady(){nextState = &FSM::st_READ_FREQUENCIES;}	///< This event sets the nextState as st_READ_FREQUENCIES */

	/******************************************************************************
	 * State list declaration
	 */
	void st_SETUP();						///< this state is used at the start of the code */
	void st_CONFIG();						///< this state is used to download or upload configuration settings */
	void st_MEASURE();						///< this state is used during the measurement process */
	void st_READ_FREQUENCIES();				///< This state is used to read frequencies from anemometers or RPMs sensors */
	void st_CALC_AVERAGES();				///< This state is used to calculate the averages once all the necessary measurements are done */
	void st_OUTPUT();						///< this state is used to save or send data to the outside */
	void st_SLEEP();						///< this state is used to put the microcontroller to sleep when it is idle  */

	/******************************************************************************
	 * Configuration management
	 ******************************************************************************/
	/// This method adds to the serial channel string the "ch" character provided in its input
	inline void addChar(char ch){	serialString[StrIndex]=ch;	StrIndex++;	}

	/// This method prints the main configuration menu
	void menu();
	/**
	 * The config method updates private members
	 * @param stringConfig This string contain the ID parameter. This value is written as "id=value".
	 * @return If the id value is wrong, this method returns "1". If everything is correct is returns a "0".
	 */
	bool config(char *stringConfig);

	/// The printConfig method prints all the current parameters and their related values on the serial port (uart0)
	void printConfig();

	/**
	 * The configDT method updates the date and the time
	 * @param stringConfig This string contain the ID parameter. This value is written as "id=value".
	 * The String for time is written as : "*21=hh:mm:ss"
	 * The String for date is written as : "*22=mm/dd/yyyy"
	 */
	void configDT(char *stringConfig);

	/// This method prints the date and the time on the serial port (uart0)
	void printDateTime();


	/// This method prints the output configuration
	void printOutput();

	/**
	 * The configOutput method updates the output configurations
	 * @param stringConfig This string contain the ID parameter. This value is written as "id=value".
	 */
	void configOutput(char *stringConfig);

	/******************************************************************************
	 * FSM parameters in EEPROM management
	 ******************************************************************************/
	/// This method loads saved parameters for the FSM from the EEPROM
	void load_param();

	/// This method updates saved parameters for the FSM into the EEPROM
	void update_param();

	/// \brief This method initialize the EEPROM memory whenever there's an incompatibility between the "DATA_STRUCTURE_VERSION" and the "structure_version".
	void initialize_param();

private:
	/******************************************************************************
	 * Params saved in eeprom
	 ******************************************************************************/
	const unsigned char DATA_STRUCTURE_VERSION = 201; /**< This parameter holds the current data structure version number*/
	unsigned long structure_version;	/**< This parameter sets an auto reset of the EEPROM data when the structure version differs from current values to prevent any bad data reading due to changes in the structure */
	unsigned char node_id;				/**< This parameter identifies each datalogger (0 - 255)*/
	unsigned char measureSampleConf;	/**< This parameter sets the measurement sampling rate (0: no measure, 1 : every 10 secs, 2: every 1 min, 3: every 10 min...) */
	unsigned char measureMax;			/**< This parameter sets the maximum number of measurements to be done. WARNING: when changing this value, the Sensor::MAX_DATA_SAMPLE function must be adjusted accordingly! */

	/******************************************************************************
	 * Private FSM members
	 ******************************************************************************/
	unsigned char m_eeprom_addr;		/**< The m_eeprom_addr parameters holds the adress within the EEPROM where the FSM data is stored */
	unsigned char isInConfig;			/**< The isInConfig parameter serves as a flag identifying the system is in config mode */
	unsigned char measure;				/**< The measure parameter serves as a measurement counter */
	unsigned char secondOld;			/**< The secondOld parameter stores the previous value of time in the form of the previous second. It is used to determine if a real second has passed. */
	unsigned char second_counter;		/**< The second_counter paremeter is used to compare to the measurePeriode interval. */
	unsigned char measurePeriode;		/**< The measurePeriode parameter is the interval between two measurements, in seconds. */

	unsigned long timestamp;			/**< The timestamp parameter is saved at each average loop and used to print on output */

	char serialString[64]={'a'};		/**< The serialString vector is used for testing characters. It is passed to the CONFIG state whenever a '\r' char appears at the serial port. */
	unsigned char StrIndex=0;			/**< The StrIndex parameter is the is serial string index */

	// output config flags
	bool serial_enable;					/**< The serial_enable boolean is set to true to write data on the Serial */
	bool sd_enable;						/**< The sd_enable boolean is set to true to write data on SD card */


	/******************************************************************************
	 * Hardware interface
	 ******************************************************************************/
	Rtc_Pcf8563 rtc;					/**< Creates a Real Time Clock instance */
	SDClass sd;                         /**< Creates a SD card instance */	                         
	Power power1 = Power(1);            /**< Creates a first Power class instance */	
	Power power2 = Power(2);            /**< Creates a second Power class instance */
	


	bool sd_init;
	const unsigned char SD_CS = 7;
	};

#endif /* FSM_H_ */
